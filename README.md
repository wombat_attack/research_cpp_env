Welcome! This README will explain how to build the research computing environment so that you can run all the cool stuff that TJ Dahlke has made inside your Docker VM.

1) Make sure you have python and Docker installed on your machine. You can find Docker here:
https://www.docker.com/community-edition#/download

2) Run:
python runIt.py

3) When prompted for a password, type:
newpass

4) Type "ls". You should see:
SUCCESSFULLY_INSIDE_DOCKER

as a file that shows up. You can delete this file.

If you dont see that result, then you are still in your local machine environment. If this happens, repeat step 2-4. That usually fixes it.

5) If you got the right output from typing "ls", then congratulations! You have successfully entered the working environment. To leave the environment and return to the local environment, just type "exit".


NOTES:
**If you take a look in the Dockerfile, you'll see that all the 3rd party libraries are found under /opt. When you write code in this environment, make sure you specify that path.

** If you want anything to show up in the home directory of the docker environment, just put it inside the "dockerhomeDIR" folder.


