
From rgc007/sep-debian
MAINTAINER Bob Clapp <bob@sep.stanford.edu>
RUN apt-get -y update &&\
    apt-get -y install python3-dev swig libboost-all-dev python3-numpy-dbg python3-pytest python3-pip libboost-dev g++ git make cmake gcc &&\
    apt-get -y clean

# Install pybind
RUN git clone https://github.com/pybind/pybind11.git /opt/pybind11/src && \
    mkdir /opt/pybind11/build &&\
    cd /opt/pybind11/build && \
    cmake -DCMAKE_INSTALL_PREFIX=/usr ../src  &&\
    make -j 4 install && \
    rm -rf /opt/pybind11/build

# Install zfp
RUN git clone https://github.com/LLNL/zfp /opt/zfp/src &&\
    mkdir -p /opt/zfp/build  &&\
    cd /opt/zfp/build  && \
    cmake -DCMAKE_INSTALL_PREFIX=/opt/zfp  ../src  && \
    make &&\
    make install && \
    rm -rf /opt/zfp/build
ENV ZFP_BASE /opt/zfp

# Install hypercube
RUN git clone http://zapad.Stanford.EDU/bob/hypercube.git /opt/hypercube/src && \
    mkdir /opt/hypercube/build && \
    cd /opt/hypercube/build &&\
    cmake -DCMAKE_INSTALL_PREFIX=/opt/hypercube ../src &&\
    make install && \
    rm -rf /opt/hypercube/build

# Install sepVector
RUN git clone http://zapad.Stanford.EDU/bob/sepVector.git /opt/sepVector/src   && \
    mkdir /opt/sepVector/build &&\
    cd /opt/sepVector/build &&\
    cmake  -DPYTHON_NUMPY_INCLUDE_DIR=/usr/lib/python3/dist-packages/numpy/core/include  -Dhypercube_DIR=/opt/hypercube/cmake  -DCMAKE_INSTALL_PREFIX=/opt/sepVector ../src &&\
    make install && \
    rm -rf /opt/sepVector/build

# Install genericIO
RUN git clone http://zapad.Stanford.EDU/bob/genericIO.git /opt/genericIO/src && \
    mkdir /opt/genericIO/build &&\
    cd /opt/genericIO/build &&\
    cmake  -DPYTHON_NUMPY_INCLUDE_DIR=/usr/lib/python3/dist-packages/numpy/core/include -DsepVector_DIR=/opt/sepVector/cmake -Dhypercube_DIR=/opt/hypercube/cmake -DSEPlib_DIR=/opt/SEP/lib -DCMAKE_INSTALL_PREFIX=/opt/genericIO ../src &&\
    make install && \
    rm -rf /opt/genericIO/build


RUN apt-get install -y vim wget libfftw3-dev

# Install FFTW3
RUN mkdir /opt/FFTW &&\
    cd /opt/FFTW; wget http://fftw.org/fftw-3.3.6-pl2.tar.gz && \
    tar -xvf fftw-3.3.6-pl2.tar.gz && \
    cd fftw-3.3.6-pl2 &&\
    ./configure --prefix=/opt/FFTW --enable-shared=yes && \
    make --jobs=8 && \
    make install

# EXPOSE Port 22 allowing X11 plotting to display
RUN apt-get install -y uuid-runtime openssh-server 
RUN uuidgen >/etc/machine-id
ADD ./start_debian.sh /start_debian.sh
RUN chmod 755 /start_debian.sh
RUN ./start_debian.sh
ADD ./sshd_config /etc/ssh/sshd_config
ENTRYPOINT ["/usr/sbin/sshd", "-D"]

# ALIASES
ADD ./.aliases /home/user/.aliases
RUN echo source /home/user/.aliases >> /home/user/.bash_profile

# PATH STUFF
RUN echo export PATH=$PATH >> /home/user/.bash_profile
RUN echo export RSF="/opt/RSF" >> /home/user/.bash_profile
RUN echo export PATH=$PATH:/opt/SEP/bin >> /home/user/.bash_profile
ENV PATH="/opt/SEP/bin:${PATH}"


