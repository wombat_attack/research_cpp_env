# #========================================================
# # BUILD ispc_base IMAGE
# From rgc007/debian-sep-8
# MAINTAINER TJ Dahlke <taylor@sep.stanford.edu>
# RUN chmod a+w -Rv /opt/SEP

# # General utilities
# RUN apt-get -y update && apt-get -y upgrade && apt-get install -y --no-install-recommends apt-utils
# RUN apt-get install -y uuid-runtime openssh-server xauth cmake make clang

# # Install ISPC compiler (Needed for thread-blocking)
# RUN mkdir /opt/ispc
# RUN cd /opt/ispc/; wget https://sourceforge.net/projects/ispcmirror/files/v1.9.1/ispc-v1.9.1-linux.tar.gz; tar -xvf ispc-v1.9.1-linux.tar.gz;
# ENV PATH="/opt/ispc/ispc-v1.9.1-linux:${PATH}"

#========================================================
# BUILD main IMAGE
From tjdahlke/research_cpp_env:ispc_base

# Install boost library and TBB
RUN apt-get update
RUN apt-get install -y libboost-all-dev libtbb-dev

# Install hypercube
RUN git clone http://zapad.Stanford.EDU/bob/hypercube.git   /opt/hypercube/src && \
    mkdir -p /opt/hypercube/build && \
    cd /opt/hypercube/build &&\
    cmake -DCMAKE_INSTALL_PREFIX=/opt/hypercube ../src &&\
    make install &&\
    rm -rf /opt/hypercube/build

# Install genericIO
RUN git clone http://zapad.Stanford.EDU/bob/genericIO.git  /opt/genericIO/src && \
    mkdir -p /opt/genericIO/build &&\
    cd /opt/genericIO/build &&\
    cmake  -Dhypercube_DIR=/opt/hypercube/lib -DSEPlib_DIR=/opt/SEP/lib -DCMAKE_INSTALL_PREFIX=/opt/genericIO ../src &&\
    make install && \
    rm -rf /opt/genericIO/build

# Install gieeSolver
RUN git clone http://zapad.Stanford.EDU/SEP-external/gieeSolver.git /opt/gieeSolver/src &&\
    mkdir -p /opt/gieeSolver/build &&\
    cd /opt/gieeSolver/build && \
    cmake -DCMAKE_INSTALL_PREFIX=/opt/gieeSolver -Dhypercube_DIR=/opt/hypercube/lib  -DgenericIO_DIR=/opt/genericIO/lib -DBoost_INCLUDE_DIR=/usr/include ../src&&\
    make install;

# Install FFTW3
RUN mkdir /opt/FFTW &&\
    cd /opt/FFTW; wget http://fftw.org/fftw-3.3.6-pl2.tar.gz && \
    tar -xvf fftw-3.3.6-pl2.tar.gz && \
    cd fftw-3.3.6-pl2; ./configure && \
    make && \
    make install

# Install additional programs
RUN apt-get update
RUN apt-get install -y vim evince scons

# EXPOSE Port 22 allowing X11 plotting to display
RUN uuidgen >/etc/machine-id
ADD ./start_debian.sh /start_debian.sh
RUN chmod 755 /start_debian.sh
RUN ./start_debian.sh
ADD ./sshd_config /etc/ssh/sshd_config
ENTRYPOINT ["/usr/sbin/sshd", "-D"]

# Install the SepTex stuff
RUN apt-get update &&\
  apt-get -y install gcc make git python &&\
  apt-get -y clean
RUN mkdir /opt/RSF
RUN git clone https://github.com/ahay/src /opt/RSF/src
RUN cd /opt/RSF/src && \
  RSFROOT=/opt/RSF ./configure&&\
  make install
RUN apt-get update
RUN apt-get -y install texlive environment-modules texlive-latex-base texlive-xetex &&\
  apt-get -y clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/man/?? /usr/share/man/??_*
RUN ln -s /opt/RSF/lib/python2.7/dist-packages/rsf/tex.py /opt/RSF/lib/python2.7/dist-packages/rsftex.py
RUN git clone http://zapad.Stanford.EDU/SEP-external/SEP-tex.git /usr/local/share/texmf/SEP
RUN rm -rf /usr/local/share/texmf/SEP/dockers
# RUN cd /usr/local/share/texmf/SEP &&  mv * .??* ../
RUN cd /usr/local/share/texmf/SEP  && cp -rf * ../. && cp -rf .git* ../.
# Get rid of unneccessary RSF stuff
RUN rm -rf /opt/RSF/share /opt/RSF/include /opt/RSF/lib/lib* #/opt/RSF/bin/* 
RUN echo export PYTHONPATH=/opt/RSF/lib/python2.7/dist-packages >> /home/user/.bash_profile
ENV PATH="/opt/RSF/bin:${PATH}"
ENV PATH="/opt/SEP/bin:${PATH}"
RUN texhash

# ALIASES
ADD ./.aliases /home/user/.aliases
RUN echo source /home/user/.aliases >> /home/user/.bash_profile

# PATH STUFF
RUN echo export PATH=$PATH >> /home/user/.bash_profile
RUN echo export RSF="/opt/RSF" >> /home/user/.bash_profile

# # Debugging stuff
# RUN chown user:user /opt/ -R
# #Set ownership of ispc stuff to user for debugging (not permanently needed)
# RUN chown user:user /opt/ -R
# RUN chown user:user /opt/ispc -R
# RUN chmod 777 /etc/ssh/sshd_config
# ENV PATH="/opt/FFTW:${PATH}"
# chown user:user /opt/FFTW -R && \

# ENV C_INCLUDE_PATH="/opt/gieeSolverI/include"
# ENV C_INCLUDE_PATH="/opt/genericIO/include:${C_INCLUDE_PATH}"
